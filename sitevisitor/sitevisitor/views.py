from django.shortcuts import render
from models import visitors, Visitors, Visitor

def index(request):
    visitors.add(Visitor.from_request(request))
    return render(request, 'index.html', {
            'markers': Visitors.latest().to_json(),
            'visitor_per_city': Visitors.latest().per_city()
        }
    )
