import logging
import datetime
from django.contrib.gis.geoip import GeoIP
from mongoengine import Document, StringField, ValidationError, \
    EmbeddedDocument
from mongoengine.fields import DateTimeField, PointField, EmbeddedDocumentField

logger = logging.getLogger(__name__)

class Location(EmbeddedDocument):
    city = StringField(max_length=50, required=True)
    point = PointField(required=True)
    country = StringField(max_length=50, required=True)

    def __str__(self):
        return ', '.join(map(str, (self.city, self.country)))


class Visitor(Document):
    ip = StringField(max_length=50, required=True)
    location = EmbeddedDocumentField(Location, required=True)
    pub_date = DateTimeField(default=datetime.datetime.now)

    @classmethod
    def from_request(cls, request):
        g = GeoIP()
        ip = request.META.get('REMOTE_ADDR', None)
        # ip = '195.138.78.125'
        location = None
        country = None
        if ip:
            country_record = g.country(ip)
            if country_record:
                country = country_record['country_name']
            city_record = g.city(ip)
            if city_record:
                location = Location(
                    city=city_record['city'],
                    point=[city_record['latitude'], city_record['longitude']],
                    country=country
                )
        return Visitor(ip=ip, location=location)

    def __str__(self):
        return ', '.join(map(str, (self.ip, self.location)))


class Visitors(object):

    def __init__(self, source=Visitor.objects.all()):
        self.source = source

    @staticmethod
    def add(visitor):
        try:
            visitor.save()
            return True
        except ValidationError:
            logger.exception('Failed to save visitor data')
        return False

    def __iter__(self):
        return iter(self.source)

    def to_json(self):
        return self.source.to_json()

    def per_city(self):
        return self.source.item_frequencies('location.city')

    @classmethod
    def latest(cls, mins=15):
        return cls(
            source=Visitor.objects.filter(
                pub_date__gte=datetime.datetime.now() - datetime.timedelta(minutes=mins)
            )
        )


visitors = Visitors()

