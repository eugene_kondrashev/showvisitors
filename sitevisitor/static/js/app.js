$(document).ready(function() {
    /*Initial Map START*/
    function initialize() {
        var myLatlng = new google.maps.LatLng(-34.397, 150.644);
        var myOptions = {
            zoom: 2,
            center: myLatlng,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        return new google.maps.Map(document.getElementById("map"), myOptions);

    }

    /*Initial Map END*/
    function markers(map, markers) {
        var self = this;
        this.map = map;
        this.markers = markers;

        this.createMarkers = function() {
            var length = self.markers.length;
            var infowindow = new google.maps.InfoWindow();
            var marker;
            for (var i = 0; i < length; i++) {
                marker = new google.maps.Marker({
                    position: new google.maps.LatLng(
                        self.markers[i].location.point.coordinates[0],
                        self.markers[i].location.point.coordinates[1]
                    ),
                    map: map
                });

                google.maps.event.addListener(
                    marker, 'mouseover',
                    (function(marker,i) {
                        return function() {
                            infowindow.setContent(
                                self.markers[i].location.city +
                                ", " +
                                self.markers[i].location.country);
                            infowindow.open(map, marker);
                        };
                    })(marker,i)
                );
                google.maps.event.addListener(
                    marker, 'mouseout',
                    (function(marker) {
                        return function() {
                            infowindow.close(map, marker);
                        };
                    })(marker)
                );
            }
            
        },        
        (this.init = function(){
            this.createMarkers();
        })();
    }

    var map = initialize();
    var markers = markers(map, window.markers);
    
    /*Events*/
    $("#icoOpenClose").click(function(){
        $(this).toggleClass("show");
        $("#panel").toggleClass("show");
        
    });
});


