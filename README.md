# Intro

This is a demo Django/MongoDB application showing site visitor
location 15 minutes statistics using google maps.

# Prerequisites
1. Download/Unzip [GeoLiteCity](http://geolite.maxmind.com/download/geoip/database/GeoLiteCity.dat.gz) to `sitevisitor/geoip`
2. Download/Unzip [GeoIP](http://geolite.maxmind.com/download/geoip/database/GeoLiteCountry/GeoIP.dat.gz) to `sitevisitor/geoip`

# Configuration
1. `virtualenv env`
2. `source env/bin/activate`
3. `pip install -r requirements.txt`
